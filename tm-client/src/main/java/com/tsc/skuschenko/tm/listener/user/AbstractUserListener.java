package com.tsc.skuschenko.tm.listener.user;

import com.tsc.skuschenko.tm.endpoint.User;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public abstract class AbstractUserListener extends AbstractListener {

    protected void showUser(final @NotNull User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

}
