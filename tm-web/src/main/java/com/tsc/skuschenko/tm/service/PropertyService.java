package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Nullable
    @Value("${jdbc.cache.factoryClass}")
    private String factoryClass;

    @Nullable
    @Value("${jdbc.dialect}")
    private String jdbcDialect;

    @NotNull
    @Value("${jdbc.driver}")
    private String jdbcDriver;

    @Nullable
    @Value("${jdbc.hbm2ddl}")
    private String jdbcHbm2ddl;

    @Nullable
    @Value("${jdbc.password}")
    private String jdbcPassword;

    @Nullable
    @Value("${jdbc.showSql}")
    private String jdbcShowSql;

    @Nullable
    @Value("${jdbc.url}")
    private String jdbcUrl;

    @Nullable
    @Value("${jdbc.userName}")
    private String jdbcUserName;

    @Nullable
    @Value("${jdbc.cache.liteMember}")
    private String liteMember;

    @Nullable
    @Value("${jdbc.cache.minimalPuts}")
    private String minimalPuts;

    @Nullable
    @Value("${jdbc.cache.needSecondLevel}")
    private String needSecondLevel;

    @Nullable
    @Value("${jdbc.cache.providerConfiguration}")
    private String providerConfiguration;

    @Nullable
    @Value("${jdbc.cache.queryCache}")
    private String queryCache;

    @Nullable
    @Value("${jdbc.cache.regionPrefix}")
    private String regionPrefix;

    @Nullable
    @Value("${jdbc.cache.secondLevel}")
    private String secondLevel;

}