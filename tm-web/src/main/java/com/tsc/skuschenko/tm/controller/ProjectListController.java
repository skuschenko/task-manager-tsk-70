package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProjectListController {

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Autowired
    @NotNull
    private IUserService userService;

    @GetMapping("/projects")
    @NotNull
    public ModelAndView index() {
        @NotNull final Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        return new ModelAndView(
                "project-list", "projects",
                projectService.findAllByUserId(UserUtil.getUserId())
        );
    }

}
