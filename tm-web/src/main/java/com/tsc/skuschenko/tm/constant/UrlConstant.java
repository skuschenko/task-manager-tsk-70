package com.tsc.skuschenko.tm.constant;

import org.jetbrains.annotations.NotNull;

public class UrlConstant {

    @NotNull
    public static final String CREATE_ALL_METHOD = "/createAll";

    @NotNull
    public static final String CREATE_METHOD = "/create";

    @NotNull
    public static final String DELETE_ALL_METHOD = "/deleteAll";

    @NotNull
    public static final String DELETE_BY_ID_METHOD = "/deleteById/{id}";

    @NotNull
    public static final String DELETE_BY_LOGIN_METHOD = "/deleteByLogin/{login}";

    @NotNull
    public static final String FIND_ALL_METHOD = "/findAll";

    @NotNull
    public static final String FIND_BY_ID_METHOD = "/findById/{id}";

    @NotNull
    public static final String LOGIN = "/login";

    @NotNull
    public static final String LOGOUT = "/logout";

    @NotNull
    public static final String PROFILE = "/profile";

    @NotNull
    public static final String SAVE_ALL_METHOD = "/saveAll";

    @NotNull
    public static final String SAVE_METHOD = "/save";

    @NotNull
    public static final String SESSION = "/session";

}
