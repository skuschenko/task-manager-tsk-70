package com.tsc.skuschenko.tm;

import com.tsc.skuschenko.tm.component.Bootstrap;
import com.tsc.skuschenko.tm.configuration.LoggerConfiguration;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public final class Application {

    public static void main(String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        LoggerConfiguration.class
                );
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}